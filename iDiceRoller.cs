using System.Collections.Generic;
interface IDiceRoller {
    void RollAllDice();
    IList<int> GetRollingResults();
}