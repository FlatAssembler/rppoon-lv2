//Related to tasks 1-5.
using System;
using System.Collections.Generic;
class DiceRoller : ILogable
{
    private List<Die> dice;
    private List<int> resultForEachRoll;
    private ILogger logger;

    public DiceRoller(ILogger newLogger=null)
    {
        this.dice = new List<Die>();
        this.resultForEachRoll = new List<int>();
        logger=newLogger;
    }
    
    public void InsertDie(Die die)
    {
        dice.Add(die);
    }
    
    public void RollAllDice()    
    {
        //clear results of previous rolling
        this.resultForEachRoll.Clear();
        foreach(Die die in dice)
        {
            this.resultForEachRoll.Add(die.Roll());
        }
    }
    
    //View of the results
    public IList<int> GetRollingResults()
    {
        return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
            this.resultForEachRoll
            );
    }
    
    public int DiceCount
    {
        get
        {
            return dice.Count;
        }
    }

    public string GetStringRepresentation(){
        string representation=resultForEachRoll.Count+" numbers: ";
        for (int i=0; i<resultForEachRoll.Count; i++)
            representation+=resultForEachRoll[i]+" ";
        return representation;
    }

}