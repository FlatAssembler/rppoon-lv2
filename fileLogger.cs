//Related to tasks 3-5.
using System;
class FileLogger : ILogger
{
    private string type;
    private string filePath;
    
    public FileLogger(string filePath)
    {
        this.type = "File";
        this.filePath = filePath;
    }
    public void Log(string message)
    {
        if (this.type.Equals("Console"))
            Console.WriteLine(message);
        else if (this.type.Equals("File"))
        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
        {
            writer.WriteLine(message);
        }
        else
            throw new ArgumentException("Unkown type");
    }
    public void Log(ILogable data) {
       using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
        {
            writer.WriteLine(data.GetStringRepresentation());
        } 
    }
}