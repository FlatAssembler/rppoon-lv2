using System;
using System.Collections.Generic;
class FlexibleDiceRoller: IDiceRoller
{
    private List<Die> dice;
    private List<int> resultForEachRoll;

    public FlexibleDiceRoller()
    {
        this.dice = new List<Die>();
        this.resultForEachRoll = new List<int>();
    }
    public void InsertDie(Die die)
    {
        dice.Add(die);
    }
    public void RemoveAllDice()
    {
        this.dice.Clear();
        this.resultForEachRoll.Clear();
    }
    public void RollAllDice()
    {
        this.resultForEachRoll.Clear();
        foreach(Die die in dice)
        {
            this.resultForEachRoll.Add(die.Roll());
        }
    }
    public void RemoveDiceWithSides(int n) {
        for (int i=0; i<dice.Count; i++)
            if (dice[i].getNumberOfSides()==n) {
                dice.RemoveAt(i);
                i--;
            }
    }
    //View of the results
    public IList<int> GetRollingResults()
    {
        return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
            this.resultForEachRoll
            );
    }
}
