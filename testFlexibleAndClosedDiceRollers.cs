using System;
using System.Collections.Generic;
class TestFlexibleAndClosedDiceRollers {
    public static List<IDiceRoller> testedRollers;
    public static void printTestedRollers(){
        foreach (IDiceRoller roller in testedRollers) {
            roller.RollAllDice();
            IList<int> result=roller.GetRollingResults();
            Console.Write(result.Count+" numbers: ");
            foreach (int die in result)
                Console.Write(die+" ");
            Console.WriteLine("");
        }
    }
    public static void Main() {
        testedRollers=new List<IDiceRoller>();
        FlexibleDiceRoller flexibleRoller=new FlexibleDiceRoller();
        testedRollers.Add(flexibleRoller);
        for (int i=4; i<=20; i++)
            flexibleRoller.InsertDie(new Die(i));
        ClosedDiceRoller closedRoller=new ClosedDiceRoller(20,6);
        testedRollers.Add(closedRoller);
        Console.WriteLine("Before removing some dice from the flexible roller:");
        printTestedRollers();
        for (int i=4; i<20; i++) {
            if (i==4 || i==6 || i==8 || i==12 || i==20) continue;
            flexibleRoller.RemoveDiceWithSides(i);
        }
        Console.WriteLine("After the removal of them:");
        printTestedRollers();
    }
}