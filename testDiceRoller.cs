//Related to tasks 1-5.
using System;
class TestDiceRoller{
    public static void Main() {
        ConsoleLogger logger=new ConsoleLogger();
        DiceRoller rollerToBeTested=new DiceRoller();
        for (int i=0; i<20; i++) {
            rollerToBeTested.InsertDie(new Die(6));
        }
        rollerToBeTested.RollAllDice();
        System.Collections.Generic.IList<int> results=rollerToBeTested.GetRollingResults();
        Console.WriteLine("The results of the rolling are:");
        logger.Log(rollerToBeTested);
    }
}