using System;
using System.Collections.Generic;
class ClosedDiceRoller: IDiceRoller
{
    private List<Die> dice;
    private List<int> resultForEachRoll;
    public ClosedDiceRoller(int diceCount, int numberOfSides)
    {
        this.dice = new List<Die>();
        for(int i = 0; i < dice.Count; i++)
        {
            this.dice.Add(new Die(numberOfSides));
        }
        this.resultForEachRoll = new List<int>();
    }
    public void RollAllDice()
    {
        this.resultForEachRoll.Clear();
        foreach(Die die in dice)
        {
            this.resultForEachRoll.Add(die.Roll());
        }
    }
    //View of the results
    public IList<int> GetRollingResults()
    {
        return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
            this.resultForEachRoll
            );
    }
}