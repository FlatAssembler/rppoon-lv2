class RandomGenerator
{
    private static RandomGenerator instance; //unique instance
    private System.Random random;
    
    private RandomGenerator(){
        this.random = new System.Random();
    }
    
    //lazy instantion
    public static RandomGenerator GetInstance()
    {
        if(instance == null)
            instance = new RandomGenerator();
        return instance;
    }

    public int NextInt(int lowerBound, int upperBound)
    {
        return random.Next(lowerBound, upperBound);
    }

}