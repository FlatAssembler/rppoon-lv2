//Related to tasks 3-5.
using System;
class ConsoleLogger : ILogger
{
    private string type;
    private string filePath;
    
    public ConsoleLogger()
    {
        this.type = "Console";
        this.filePath = "/dev/tty";
    }
    public void Log(string message)
    {
        if (this.type.Equals("Console"))
            Console.WriteLine(message);
        else if (this.type.Equals("File"))
        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
        {
            writer.WriteLine(message);
        }
        else
            throw new ArgumentException("Unkown type");
    }
    public void Log(ILogable data) {
        Console.WriteLine(data.GetStringRepresentation()); 
    }
}