//Related to tasks 3-5.
interface ILogger
{
    void Log(ILogable data);
    void Log(string message);
}