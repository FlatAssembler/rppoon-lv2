//Related to task #5.
interface ILogable
{
    string GetStringRepresentation();
}